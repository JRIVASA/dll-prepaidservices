VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmPhoneProducts 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   11490
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11490
   ScaleMode       =   0  'User
   ScaleWidth      =   15330
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FrameStellarBar 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   570
      Left            =   0
      TabIndex        =   41
      Top             =   0
      Width           =   15675
      Begin VB.Image CmdClose 
         Height          =   480
         Left            =   14640
         Picture         =   "FrmPhoneProducts.frx":0000
         Top             =   45
         Width           =   480
      End
      Begin VB.Label LbWebsite 
         BackColor       =   &H00404040&
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   360
         TabIndex        =   42
         Top             =   120
         Width           =   2295
      End
   End
   Begin VB.Timer TimerCheckCombo 
      Interval        =   25
      Left            =   12960
      Top             =   960
   End
   Begin VB.Timer TimerLoadProducts 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   12480
      Top             =   960
   End
   Begin VB.Frame FrameCarrierFilters 
      Caption         =   "                                                                Filter by Carrier Initials"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000006&
      Height          =   2595
      Left            =   170
      TabIndex        =   5
      Top             =   1920
      Visible         =   0   'False
      Width           =   15015
      Begin VB.CheckBox CmdTopSold 
         BackColor       =   &H00666666&
         Caption         =   "TOP"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   17.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   900
         Left            =   12600
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   1485
         Width           =   2295
      End
      Begin VB.CheckBox CmdAll 
         BackColor       =   &H00666666&
         Caption         =   "ALL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   17.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   900
         Left            =   13560
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   480
         Width           =   1335
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   2
         Left            =   2040
         Picture         =   "FrmPhoneProducts.frx":1D82
         Style           =   1  'Graphical
         TabIndex        =   32
         Tag             =   "C"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   7
         Left            =   6840
         Picture         =   "FrmPhoneProducts.frx":3308
         Style           =   1  'Graphical
         TabIndex        =   31
         Tag             =   "H"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   15
         Left            =   1080
         Picture         =   "FrmPhoneProducts.frx":488E
         Style           =   1  'Graphical
         TabIndex        =   30
         Tag             =   "o"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   24
         Left            =   9720
         Picture         =   "FrmPhoneProducts.frx":5E14
         Style           =   1  'Graphical
         TabIndex        =   29
         Tag             =   "X"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   6
         Left            =   5880
         Picture         =   "FrmPhoneProducts.frx":739A
         Style           =   1  'Graphical
         TabIndex        =   28
         Tag             =   "G"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   8
         Left            =   7800
         Picture         =   "FrmPhoneProducts.frx":8920
         Style           =   1  'Graphical
         TabIndex        =   27
         Tag             =   "i"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   23
         Left            =   8760
         Picture         =   "FrmPhoneProducts.frx":9EA6
         Style           =   1  'Graphical
         TabIndex        =   26
         Tag             =   "w"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   26
         Left            =   11640
         Picture         =   "FrmPhoneProducts.frx":B42C
         Style           =   1  'Graphical
         TabIndex        =   25
         Tag             =   "Z"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   5
         Left            =   4920
         Picture         =   "FrmPhoneProducts.frx":C9B2
         Style           =   1  'Graphical
         TabIndex        =   24
         Tag             =   "F"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   21
         Left            =   6840
         Picture         =   "FrmPhoneProducts.frx":DF38
         Style           =   1  'Graphical
         TabIndex        =   23
         Tag             =   "u"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BackColor       =   &H00808080&
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   17
         Left            =   3000
         Picture         =   "FrmPhoneProducts.frx":F4BE
         Style           =   1  'Graphical
         TabIndex        =   22
         Tag             =   "q"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   4
         Left            =   3960
         Picture         =   "FrmPhoneProducts.frx":10A44
         Style           =   1  'Graphical
         TabIndex        =   21
         Tag             =   "e"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   11
         Left            =   10680
         Picture         =   "FrmPhoneProducts.frx":11FCA
         Style           =   1  'Graphical
         TabIndex        =   20
         Tag             =   "L"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   19
         Left            =   4920
         Picture         =   "FrmPhoneProducts.frx":13550
         Style           =   1  'Graphical
         TabIndex        =   19
         Tag             =   "S"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   10
         Left            =   9720
         Picture         =   "FrmPhoneProducts.frx":14AD6
         Style           =   1  'Graphical
         TabIndex        =   18
         Tag             =   "K"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   0
         Left            =   120
         Picture         =   "FrmPhoneProducts.frx":1605C
         Style           =   1  'Graphical
         TabIndex        =   17
         Tag             =   "A"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   20
         Left            =   5880
         Picture         =   "FrmPhoneProducts.frx":175E2
         Style           =   1  'Graphical
         TabIndex        =   16
         Tag             =   "t"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   22
         Left            =   7800
         Picture         =   "FrmPhoneProducts.frx":18B68
         Style           =   1  'Graphical
         TabIndex        =   15
         Tag             =   "V"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   9
         Left            =   8760
         Picture         =   "FrmPhoneProducts.frx":1A0EE
         Style           =   1  'Graphical
         TabIndex        =   14
         Tag             =   "J"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   16
         Left            =   2040
         Picture         =   "FrmPhoneProducts.frx":1B674
         Style           =   1  'Graphical
         TabIndex        =   13
         Tag             =   "p"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   18
         Left            =   3960
         Picture         =   "FrmPhoneProducts.frx":1CBFA
         Style           =   1  'Graphical
         TabIndex        =   12
         Tag             =   "r"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   25
         Left            =   10680
         Picture         =   "FrmPhoneProducts.frx":1E180
         Style           =   1  'Graphical
         TabIndex        =   11
         Tag             =   "y"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   1
         Left            =   1080
         Picture         =   "FrmPhoneProducts.frx":1F706
         Style           =   1  'Graphical
         TabIndex        =   10
         Tag             =   "B"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   13
         Left            =   12600
         Picture         =   "FrmPhoneProducts.frx":20C8C
         Style           =   1  'Graphical
         TabIndex        =   9
         Tag             =   "N"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   14
         Left            =   120
         Picture         =   "FrmPhoneProducts.frx":22212
         Style           =   1  'Graphical
         TabIndex        =   8
         Tag             =   "�"
         Top             =   1485
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   3
         Left            =   3000
         Picture         =   "FrmPhoneProducts.frx":23798
         Style           =   1  'Graphical
         TabIndex        =   7
         Tag             =   "D"
         Top             =   480
         Width           =   900
      End
      Begin VB.CommandButton CmdKey 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   12
         Left            =   11640
         Picture         =   "FrmPhoneProducts.frx":24D1E
         Style           =   1  'Graphical
         TabIndex        =   6
         Tag             =   "M"
         Top             =   480
         Width           =   900
      End
      Begin VB.Line LineFrameCarrier 
         BorderColor     =   &H8000000A&
         X1              =   25
         X2              =   5875
         Y1              =   170
         Y2              =   170
      End
      Begin VB.Shape ShapeSelected 
         BackColor       =   &H000000FF&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H000000FF&
         BorderWidth     =   2
         Height          =   945
         Left            =   12570
         Top             =   1470
         Width           =   2355
      End
   End
   Begin VB.PictureBox PicLoad 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   1560
      ScaleHeight     =   495
      ScaleWidth      =   2415
      TabIndex        =   4
      Top             =   9240
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.TextBox CmbCountriesPlaceholder 
      Alignment       =   2  'Center
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000006&
      Height          =   550
      Left            =   4230
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   3
      Text            =   "FrmPhoneProducts.frx":262A4
      Top             =   1230
      Width           =   6350
   End
   Begin VB.CommandButton CmdExit 
      Caption         =   "Back"
      Height          =   975
      Left            =   4080
      TabIndex        =   2
      Top             =   9240
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.ComboBox CmbCountries 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4200
      Sorted          =   -1  'True
      TabIndex        =   1
      Text            =   "Country / Pa�s"
      Top             =   1200
      Visible         =   0   'False
      Width           =   6735
   End
   Begin VB.TextBox TxtJson 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   1560
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Text            =   "FrmPhoneProducts.frx":262C9
      Top             =   9840
      Visible         =   0   'False
      Width           =   2415
   End
   Begin MSFlexGridLib.MSFlexGrid GridEvitarFoco 
      Height          =   30
      Left            =   18720
      TabIndex        =   34
      Top             =   360
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   53
      _Version        =   393216
      BackColor       =   -2147483633
      BackColorSel    =   -2147483633
      BackColorBkg    =   -2147483633
   End
   Begin VB.Frame FrameNext 
      BorderStyle     =   0  'None
      Height          =   2955
      Left            =   14070
      TabIndex        =   35
      Top             =   4320
      Visible         =   0   'False
      Width           =   1260
      Begin VB.CommandButton CmdNext 
         Enabled         =   0   'False
         Height          =   1455
         Left            =   0
         Picture         =   "FrmPhoneProducts.frx":262DB
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   720
         Width           =   1270
      End
   End
   Begin VB.Frame FrameBack 
      BorderStyle     =   0  'None
      Height          =   2955
      Left            =   0
      TabIndex        =   36
      Top             =   4320
      Visible         =   0   'False
      Width           =   1260
      Begin VB.CommandButton CmdBack 
         CausesValidation=   0   'False
         Enabled         =   0   'False
         Height          =   1455
         Left            =   0
         Picture         =   "FrmPhoneProducts.frx":2CABD
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   720
         Width           =   1270
      End
   End
   Begin VB.Label lblTeclado 
      BackColor       =   &H8000000A&
      BackStyle       =   0  'Transparent
      Height          =   1005
      Left            =   11370
      TabIndex        =   39
      Top             =   960
      Width           =   1455
   End
   Begin VB.Image CmdTeclado 
      Height          =   600
      Left            =   11760
      MouseIcon       =   "FrmPhoneProducts.frx":3329F
      MousePointer    =   99  'Custom
      Picture         =   "FrmPhoneProducts.frx":335A9
      Stretch         =   -1  'True
      Top             =   1200
      Width           =   600
   End
   Begin VB.Image ImgProduct 
      Height          =   2115
      Index           =   0
      Left            =   1680
      Stretch         =   -1  'True
      Top             =   5475
      Visible         =   0   'False
      Width           =   3555
   End
End
Attribute VB_Name = "FrmPhoneProducts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ProductType As PhoneServices_ProductType
Public ProductDataReady As Boolean
Public ProductData As Object
Public FilteredData As Object
Public FilteredText As String
Public ProductResponse As String

Public TopSoldProductsData As Object
Public SelectedProductData As Object

Private CountryList As New Dictionary

Private SelectedLetter As String

Private CmbCountries_SelectedIndex As Long
Private CmbCountries_ListBoxHwnd As Long
Private DropDownVisible As Boolean

Private UIReady As Boolean

Private CurrentButtonPanelIndex As Integer

Private InitialButtonPanelMargin As Long

Private ButtonTopMargin As Long
Private ButtonLeftMargin As Long
Private ButtonHeight As Long
Private ButtonWidth As Long

Private TmpRowHeight As Long
Private TmpColWidth As Long

Private ButtonMatrix As Collection

Private LoadingProducts As Boolean

Private Sub CrearMatrizDeBotones(ByVal ButtonPanelItems As Integer)
    
    On Error GoTo Error
    
    Dim ButtonPanel As Collection
    
    If Not ButtonMatrix Is Nothing Then Exit Sub
    
    Set ButtonMatrix = New Collection
    
    Dim i As Integer
    
    i = 1
    
    While Not i > ImgProduct.UBound
        
        DoEvents
        
        If ButtonPanel Is Nothing Then Set ButtonPanel = New Collection
        
        ButtonPanel.Add i
        
        If (i Mod ButtonPanelItems) = 0 Or i = ImgProduct.UBound Then ButtonMatrix.Add ButtonPanel: Set ButtonPanel = Nothing
        
        i = i + 1
        
    Wend
    
    FrameBack.Visible = True
    FrameNext.Visible = True
    FrameBack.Enabled = False
    CmdBack.Enabled = False
    CmdBack.ZOrder 0
    FrameNext.Enabled = True
    CmdNext.Enabled = True
    CmdNext.ZOrder 0
    CurrentButtonPanelIndex = 1
    
    Exit Sub
    
Error:
    
    Debug.Print Err.Description
    
End Sub

Private Sub OrganizarBotones(Optional SetPanelIndex As Boolean = False)
    
    'MousePointer = vbHourglass
    
    Dim StartIndex As Integer, EndIndex As Integer
    
    If SetPanelIndex Then
        StartIndex = ButtonMatrix(CurrentButtonPanelIndex)(1)
        EndIndex = ButtonMatrix(CurrentButtonPanelIndex)(ButtonMatrix(CurrentButtonPanelIndex).Count)
    Else
        StartIndex = 1
        EndIndex = ImgProduct.UBound
    End If
    
    If CmbCountries.Visible Then
        
        InitialButtonPanelMargin = 1720
        
        ButtonTopMargin = 360
        ButtonLeftMargin = 540
        ButtonWidth = 3600
        ButtonHeight = 2220
        
        TmpRowHeight = 2800 '1800
        TmpColWidth = InitialButtonPanelMargin - ButtonLeftMargin
        
    Else
            
        InitialButtonPanelMargin = 1720
            
        ButtonTopMargin = 250
        ButtonLeftMargin = 540
        ButtonWidth = 3600
        ButtonHeight = 2220
        
        TmpRowHeight = 3750 '2950
        TmpColWidth = InitialButtonPanelMargin - ButtonLeftMargin
        
    End If
    
    For i = StartIndex To EndIndex
        'DoEvents
        TmpColWidth = AddButtonToRow(i, TmpColWidth)
        If (TmpRowHeight + ButtonHeight) > Me.Height Then
            CrearMatrizDeBotones i - 1
            Exit For
        End If
    Next i
    
    MousePointer = MousePointerConstants.vbDefault
    
End Sub

Private Function AddButtonToRow(ByVal pIndex As Integer, ByRef pPosicionAnterior As Long) As Long
    
    If (pPosicionAnterior + ButtonLeftMargin + ButtonWidth) > Me.Width Then
        TmpRowHeight = TmpRowHeight + ButtonTopMargin + ButtonHeight
        pPosicionAnterior = InitialButtonPanelMargin - ButtonLeftMargin
    End If
    
    If (TmpRowHeight + ButtonHeight) > Me.Height Then AddButtonToRow = pPosicionAnterior: Exit Function
    
    LoadFileToControl PicLoad, FindPath(ProductType & "\" & FilteredData(pIndex)("Code") & "\" & "ProductImage.png", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_ProductImageDir)
    Set ImgProduct(pIndex).Picture = PicLoad.Picture
    
    ImgProduct(pIndex).Top = TmpRowHeight
    
    ImgProduct(pIndex).Left = ButtonLeftMargin + pPosicionAnterior
    
    ImgProduct(pIndex).Height = ButtonHeight
    
    ImgProduct(pIndex).Width = ButtonWidth

    'PicLoad.Width = ButtonWidth
    'PicLoad.Height = ButtonHeight
    
    ImgProduct(pIndex).Visible = True
        
    AddButtonToRow = ImgProduct(pIndex).Left + ImgProduct(pIndex).Width
    
End Function

Private Sub LimpiarBotones()

    On Error Resume Next

    For i = 1 To ImgProduct.UBound
        Unload ImgProduct(i)
    Next i
    
    FrameBack.Visible = False: FrameNext.Visible = False
    
End Sub

Private Sub SetPlaceholder(Visible As Boolean, Optional Focus As Boolean = True)
    With CmbCountriesPlaceholder
    
    If Visible Then
        .Visible = True
        .ZOrder 0
        If PuedeObtenerFoco(CmbCountries) Then CmbCountries.SetFocus
    Else
        .Visible = False
        CmbCountries.ZOrder 0
    End If
    
    End With
End Sub

Private Function ProductBelongsInTopSold(ProductCode As String) As Boolean

    For Each Product In TopSoldProductsData
        If UCase(Product("ProductCode")) = UCase(ProductCode) Then
            ProductBelongsInTopSold = True
            Exit Function
        End If
    Next

End Function

Private Sub FilterProducts()
        
    LoadingProducts = True
    
    Dim SelectedCountry As String, SelectedCountryCode As String
    Dim FilterCarriersByLetter As Boolean, FilterProductsByCountry As Boolean
    
    FilterCarriersByLetter = (SelectedLetter <> vbNullString)
    FilterProductsByCountry = CmbCountries.Visible
    
    SelectedCountry = UCase(CmbCountries.Text)
    
    If SelectedCountry Like "*%*" Then
            
        Set FilteredData = New Collection
        
        For Each Product In ProductData
            DoEvents
            If IIf(FilterProductsByCountry, (UCase(Product("Country")) Like Replace(SelectedCountry, "%", "*")), True) And _
            IIf(FilterCarriersByLetter, Left(Trim(UCase(Product("CarrierName"))), 1) = UCase(SelectedLetter), True) _
            Then
                FilteredData.Add Product
            End If
        Next
        
        If FilteredData.Count = 0 Then
            For Each Product In ProductData
                DoEvents
                If IIf(FilterProductsByCountry, UCase(Product("CountryCode")) Like Replace(SelectedCountry, "%", "*"), True) And _
                IIf(FilterCarriersByLetter, Left(Trim(UCase(Product("CarrierName"))), 1) = UCase(SelectedLetter), True) _
                Then
                    FilteredData.Add Product
                End If
            Next
        End If
        
    ElseIf (SelectedCountry <> UCase("[Any Country]") And Not SelectedCountry = vbNullString) Or (SelectedCountry <> vbNullString And SelectedCountry <> UCase("[Any Country]")) Then
        
        If CountryList.Exists(UCase(SelectedCountry)) Then
                
            SelectedCountryCode = CountryList(UCase(SelectedCountry))("Code")
            
            Set FilteredData = New Collection
            
            For Each Product In ProductData
                DoEvents
                If IIf(FilterProductsByCountry, Product("CountryCode") = SelectedCountryCode, True) And _
                IIf(FilterCarriersByLetter, Left(Trim(UCase(Product("CarrierName"))), 1) = UCase(SelectedLetter), True) _
                Then
                    FilteredData.Add Product
                End If
            Next
            
        Else
            
            Set FilteredData = New Collection
            
            If (SelectedCountry = UCase("[Top Sold Products]") Or SelectedLetter = UCase("Top")) Then
            
                For Each Product In ProductData
                    DoEvents
                    If ProductBelongsInTopSold(CStr(Product("Code"))) Then
                        FilteredData.Add Product
                    End If
                Next
            
            Else
            
                For Each Product In ProductData
                    DoEvents
                    If IIf(FilterProductsByCountry, UCase(Product("Country")) = SelectedCountry, True) And _
                    IIf(FilterCarriersByLetter, Left(Trim(UCase(Product("CarrierName"))), 1) = UCase(SelectedLetter), True) _
                    Then
                        FilteredData.Add Product
                    End If
                Next
                
                If FilteredData.Count = 0 Then
                    DoEvents
                    For Each Product In ProductData
                        If IIf(FilterProductsByCountry, UCase(Product("CountryCode")) = SelectedCountry, True) And _
                        IIf(FilterCarriersByLetter, Left(Trim(UCase(Product("CarrierName"))), 1) = UCase(SelectedLetter), True) _
                        Then
                            FilteredData.Add Product
                        End If
                    Next
                End If
                                    
            End If
                                    
        End If
    
    Else
        
        Set FilteredData = New Collection
        
        For Each Product In ProductData
            DoEvents
            If IIf(FilterCarriersByLetter, Left(Trim(UCase(Product("CarrierName"))), 1) = UCase(SelectedLetter), True) Then
                FilteredData.Add Product
            End If
        Next
        
    End If
    
    'If FilteredData.Count > 0 Then
        'FilteredText = JSON.toString(FilteredData)
    'Else
        'FilteredText = vbNullString
    'End If
    
    'TxtJson.Text = FilteredText
    
    LoadingProducts = False
    
    TimerLoadProducts.Enabled = False
    TimerLoadProducts.Enabled = True
    
End Sub

Private Sub CmbCountries_Change()
    
    If Not UIReady Then Exit Sub
    
    CmbCountries.SelLength = 0
    
    Dim SelectedCountry As String
        
    SelectedCountry = UCase(CmbCountries.Text)
        
    If CmbCountries.Text = vbNullString Or CmbCountries.Text = "[Any Country]" Then
        CmbCountries_SelectedIndex = -1
        SetPlaceholder True
    Else
        SetPlaceholder False
    End If
    
    FilterProducts
    
End Sub

Private Sub CmbCountries_Click()
    
    If Not UIReady Then Exit Sub
    
    If CmbCountries_SelectedIndex <> CmbCountries.ListIndex Then
        CmbCountries_SelectedIndex = CmbCountries.ListIndex
        
        'Change
        CmbCountries_Change
        
    End If
    
End Sub

Private Sub CmbCountries_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        CmbCountries_Change
    End If
End Sub

Private Sub CmbCountries_KeyPress(KeyAscii As Integer)
    
    CmbCountries_SelectedIndex = -1
        
    Select Case KeyAscii
    
        Case vbKeyF4, vbKeyBack, vbKeyEscape
            ' Ignorar
        
        Case vbKeyReturn
            'CmbCountries_Change
        
        Case Else

            If Not DropDownVisible Then
                SendKeys "{F4}", 100
            End If
    
    End Select
    
End Sub

Private Sub CmbCountries_LostFocus()
    If CmbCountries.Text = vbNullString Or CmbCountries.Text = "[Any Country]" Then SetPlaceholder True, False
End Sub

Private Sub CmbCountriesPlaceholder_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    SetPlaceholder False
    If PuedeObtenerFoco(CmbCountries) Then CmbCountries.SetFocus
End Sub

Private Sub SetSelectedButton(pControl)
    
    ShapeSelected.Width = pControl.Width + 50
    ShapeSelected.Height = pControl.Height + 60
    ShapeSelected.Left = pControl.Left - 25
    ShapeSelected.Top = pControl.Top - 25
    
    pControl.ZOrder 0
    
End Sub

Private Sub CmdAll_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)

' No la indento debido a que esto solo es una l�nea de control
' Para que se comporte como un bot�n regular.
' El c�digo del metodo es lo que hay adentro.
'If CmdAll.Value = vbChecked Then
    
    SetSelectedButton CmdAll
    
    DoEvents
    If PuedeObtenerFoco(GridEvitarFoco) Then GridEvitarFoco.SetFocus ' L�nea de Control.
    CmdAll.Value = vbUnchecked ' L�nea de Control.
    DoEvents
    
    SelectedLetter = vbNullString
    
    FilterProducts
    
'End If

End Sub

Private Sub CmdBack_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    PreviousButtonPanel
End Sub

Private Sub CmdClose_Click()
    Unload Me
End Sub

Private Sub CmdKey_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    SelectedLetter = UCase(CmdKey(Index).Tag)
    SetSelectedButton CmdKey(Index)
    FilterProducts
End Sub

Private Sub PreviousButtonPanel()
    If CurrentButtonPanelIndex > 1 Then
        CurrentButtonPanelIndex = CurrentButtonPanelIndex - 1
        If CurrentButtonPanelIndex = 1 Then FrameBack.Enabled = False: CmdBack.Enabled = False
        DisplayButtonPanel
        FrameNext.Enabled = True: CmdNext.Enabled = True
    End If
End Sub

Private Sub NextButtonPanel()
    If CurrentButtonPanelIndex < ButtonMatrix.Count Then
        CurrentButtonPanelIndex = CurrentButtonPanelIndex + 1
        If CurrentButtonPanelIndex = ButtonMatrix.Count Then FrameNext.Enabled = False: CmdNext.Enabled = False
        DisplayButtonPanel
        FrameBack.Enabled = True: CmdBack.Enabled = True
    End If
End Sub

Private Sub DisplayButtonPanel()

    For i = ImgProduct.LBound To ImgProduct.UBound
        ImgProduct(i).Visible = False
    Next i
    
    OrganizarBotones True

End Sub

Private Sub CmdNext_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    NextButtonPanel
End Sub

Private Sub CmdTeclado_Click()
    TecladoWindows CmbCountries
End Sub

Private Sub CmdTopSold_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)

' No la indento debido a que esto solo es una l�nea de control
' Para que se comporte como un bot�n regular.
' El c�digo del metodo es lo que hay adentro.
'If CmdTopSold.Value = vbChecked Then
    
    SetSelectedButton CmdTopSold
    
    DoEvents
    If PuedeObtenerFoco(GridEvitarFoco) Then GridEvitarFoco.SetFocus ' L�nea de Control.
    CmdTopSold.Value = vbUnchecked ' L�nea de Control.
    DoEvents
    
    SelectedLetter = UCase("Top")
    
    FilterProducts
    
'End If

End Sub

Private Sub Form_Activate()
                
    If Not UIReady Then
        
        UIReady = True
        
        DoEvents
            
        If Not ProductDataReady Then
            Mensaje True, "There is a problem loading the configuration. Please try again later."
            Unload Me
            Exit Sub
        Else
            'TxtJson.Text = ProductResponse
        End If
        
        'Debug.Print ProductData.Item(1).Item("Denominations").Item(1)
        
        If ProductType = TypeInternationalTopup Then
            'SetPlaceholder True
            CmdAll_MouseUp 0, 0, 0, 0
        Else
            CmdTopSold_MouseUp 0, 0, 0, 0
        End If
        
    End If
    
End Sub

Private Sub Form_Load()

    On Error GoTo ErrLoad
    
    ProductResponse = LoadTextFile(FindPath("ProductData.json", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_ProductDataDir & "/" & ProductType))
    
    Set ProductData = JSON.parse(ProductResponse)
    
    ProductDataReady = Not (ProductData Is Nothing)
    
    ProductResponse = LoadTextFile(FindPath("TopSoldItems.json", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_TopSoldProductsDir))
    
    Set TopSoldProductsData = JSON.parse(ProductResponse)
    
    'ProductDataReady = ProductDataReady And (Not (TopSoldProductsData Is Nothing))
    ' Comentado en caso de que esto venga vac�o.
    
    If TopSoldProductsData Is Nothing Then Set TopSoldProductsData = New Collection
    
    If ProductType = TypeInternationalTopup Then
    
        CmbCountries.Visible = True
    
        CmbCountries_SelectedIndex = -1
                
'        With CmbCountries
'            .Clear
'
'            .AddItem "[Any Country]"
'            .AddItem "United States"
'            .AddItem "Nigeria"
'            .AddItem "Ghana"
'            .AddItem "Brazil"
'            .AddItem "Colombia"
'            .AddItem "Antigua"
'            .AddItem "India"
'            .AddItem "El Salvador"
'            .AddItem "Peru"
'            .AddItem "Barbados"
'            .AddItem "Etc"
'        End With
'
'        Set CountryData = New Collection
'        CountryData.Add UCase("United States"), "Name"
'        CountryData.Add "US", "Code"
'        CountryList.Add CountryData("Name"), CountryData
'
'        Set CountryData = New Collection
'        CountryData.Add UCase("Ghana"), "Name"
'        CountryData.Add "GH", "Code"
'        CountryList.Add CountryData("Name"), CountryData
'
'        Set CountryData = New Collection
'        CountryData.Add UCase("Nigeria"), "Name"
'        CountryData.Add "NG", "Code"
'        CountryList.Add CountryData("Name"), CountryData
'
'        Set CountryData = New Collection
'        CountryData.Add UCase("Brazil"), "Name"
'        CountryData.Add "BR", "Code"
'        CountryList.Add CountryData("Name"), CountryData
        
        With CmbCountries
        
            .Clear
            
            On Error GoTo ErrLoadCountries
            
            Dim CountryCollection As Object, CountryData As Collection
            Dim TmpItem As Variant, CountryJSON As String
            
            CountryJSON = LoadTextFile(FindPath("InternationalCountryData.json", _
            NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_ProductDataDir))
            
            Set CountryCollection = JSON.parse(CountryJSON)
            
            .AddItem "[Top Sold Products]"
            
            For Each TmpItem In CountryCollection
                                                
                Set CountryData = New Collection
                
                CountryData.Add TmpItem("Name"), "Name"
                CountryData.Add TmpItem("Code"), "Code"
                
                CountryList.Add CountryData("Name"), CountryData
                
                .AddItem TmpItem("Name")
                
            Next
            
            .AddItem "[Any Country]"
            
            .ListIndex = .ListCount - 1
            
        End With
        
        GoTo ContinueAfterLoadCountries
        
ErrLoadCountries:
    GoTo ContinueAfterLoadCountries
ContinueAfterLoadCountries:
        
        'FrameCarrierFilters.Top = 960
        FrameCarrierFilters.Visible = False 'True
        
        CmdTeclado.Visible = True
        lblTeclado.Visible = True
        
    Else
                
        CmdTeclado.Visible = False
        lblTeclado.Visible = False
        CmbCountries.Visible = False
        FrameCarrierFilters.Visible = True
        FrameCarrierFilters.Top = 735 ' 165
        
    End If
    
    CmbCountries_ListBoxHwnd = GetComboListHandle(CmbCountries)
    
    Call AjustarPantalla(Me)
    
    Exit Sub
    
ErrLoad:
        
    Debug.Print Err.Description
        
    Err.Clear

End Sub

Private Sub Form_Unload(Cancel As Integer)
    DoEvents
End Sub

Private Sub FrameBack_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    CmdBack_MouseUp 0, 0, 0, 0
End Sub

Private Sub FrameNext_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    CmdNext_MouseUp 0, 0, 0, 0
End Sub

Private Sub ImgProduct_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then
        
        Set SelectedProductData = FilteredData(Index)
        'MsgBox SelectedProductData("CarrierName")
                
        Set FrmPhoneProducts_Order.ProductData = SelectedProductData
        Set FrmPhoneProducts_Order.ProductImage = ImgProduct(Index).Picture
        FrmPhoneProducts_Order.ProductType = ProductType
        FrmPhoneProducts_Order.Show vbModal
        Set FrmPhoneProducts_Order = Nothing
        
    Else
        ' Let Right Click automatically display Tooltip...
    End If
End Sub

Private Sub lblTeclado_Click()
    CmdTeclado_Click
End Sub

Private Sub PicTecladoKey_Click(Index As Integer)

End Sub

Private Sub TimerCheckCombo_Timer()
    DropDownVisible = CBool(IsWindowVisible(CmbCountries_ListBoxHwnd))
End Sub

Private Sub TimerLoadProducts_Timer()
    
    On Error GoTo ErrorLoading
    
    MousePointer = MousePointerConstants.vbArrowHourglass
    
    TimerLoadProducts.Enabled = False
    
    LimpiarBotones
    
    For i = 1 To FilteredData.Count
        
        DoEvents
                
        Load ImgProduct(i)
        
        ImgProduct(i).Tag = i
        ImgProduct(i).ToolTipText = FilteredData(i)("CarrierName")
        
        'LoadFileToControl PicLoad, FindPath(ProductType & "\" & FilteredData(i)("Code") & "\" & "ProductImage.png", _
        NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_ProductImageDir)
        'Set ImgProduct(i).Picture = PicLoad.Picture
        
    Next i
    
    Set ButtonMatrix = Nothing
    
    OrganizarBotones
    
ErrorLoading:
    
    Debug.Print Err.Description
    
End Sub
