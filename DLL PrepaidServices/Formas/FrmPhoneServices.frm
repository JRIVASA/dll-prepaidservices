VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Begin VB.Form FrmPhoneServices 
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   11490
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11490
   ScaleWidth      =   15330
   Begin VB.Frame FrameDivCategories 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   570
      Left            =   0
      TabIndex        =   8
      Top             =   795
      Width           =   15675
      Begin VB.Label lblTitle1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "View and select from one of these categories"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   480
         Left            =   0
         TabIndex        =   9
         Top             =   20
         Width           =   15360
      End
   End
   Begin VB.CommandButton CmdNext 
      Enabled         =   0   'False
      Height          =   1455
      Left            =   14070
      Picture         =   "FrmPhoneServices.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   9480
      Visible         =   0   'False
      Width           =   1270
   End
   Begin VB.CommandButton CmdBack 
      CausesValidation=   0   'False
      Enabled         =   0   'False
      Height          =   1455
      Left            =   0
      Picture         =   "FrmPhoneServices.frx":67E2
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   9480
      Visible         =   0   'False
      Width           =   1270
   End
   Begin VB.PictureBox PicLoad 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   8760
      ScaleHeight     =   495
      ScaleWidth      =   2415
      TabIndex        =   4
      Top             =   30
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.Frame FrameDivTopSold 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   690
      Left            =   0
      TabIndex        =   3
      Top             =   8280
      Width           =   15675
      Begin VB.Label lblTitle2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Choose one of our Top Products"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   26.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   600
         Left            =   0
         TabIndex        =   7
         Top             =   0
         Width           =   15360
      End
   End
   Begin VB.Frame FrameStellarBar 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   570
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   15675
      Begin VB.Label LbWebsite 
         BackColor       =   &H00404040&
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   120
         Width           =   2295
      End
      Begin VB.Image CmdClose 
         Height          =   480
         Left            =   14640
         Picture         =   "FrmPhoneServices.frx":CFC4
         Top             =   45
         Width           =   480
      End
   End
   Begin VB.Timer TimerKeepAnimation 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   7350
      Top             =   1200
   End
   Begin SHDocVwCtl.WebBrowser WebDoc 
      Height          =   480
      Left            =   7275
      TabIndex        =   0
      Top             =   1800
      Width           =   600
      ExtentX         =   1058
      ExtentY         =   847
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin VB.Image ImgProductDesignArea 
      Height          =   1755
      Index           =   3
      Left            =   10920
      Stretch         =   -1  'True
      Top             =   9360
      Visible         =   0   'False
      Width           =   2835
   End
   Begin VB.Image ImgProductDesignArea 
      Height          =   1755
      Index           =   2
      Left            =   7820
      Stretch         =   -1  'True
      Top             =   9360
      Visible         =   0   'False
      Width           =   2835
   End
   Begin VB.Image ImgProductDesignArea 
      Height          =   1755
      Index           =   1
      Left            =   4700
      Stretch         =   -1  'True
      Top             =   9360
      Visible         =   0   'False
      Width           =   2835
   End
   Begin VB.Image ImgProduct 
      Height          =   1755
      Index           =   0
      Left            =   1550
      Stretch         =   -1  'True
      Top             =   9360
      Visible         =   0   'False
      Width           =   2835
   End
   Begin VB.Image ImgInternationalTopup 
      Height          =   6495
      Left            =   120
      Picture         =   "FrmPhoneServices.frx":ED46
      Stretch         =   -1  'True
      Top             =   1560
      Width           =   5655
   End
   Begin VB.Image ImgDomesticTopup 
      Height          =   6495
      Left            =   5880
      Picture         =   "FrmPhoneServices.frx":1A132
      Stretch         =   -1  'True
      Top             =   1560
      Width           =   5655
   End
   Begin VB.Image ImgPinless 
      Height          =   3195
      Left            =   11640
      Picture         =   "FrmPhoneServices.frx":21695
      Stretch         =   -1  'True
      Top             =   1560
      Width           =   3615
   End
   Begin VB.Image ImgLongDistance 
      Height          =   3195
      Left            =   11640
      Picture         =   "FrmPhoneServices.frx":25FBA
      Stretch         =   -1  'True
      Top             =   4845
      Width           =   3600
   End
End
Attribute VB_Name = "FrmPhoneServices"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private AllProductsData As Object
Private TopSoldProductsData As Object
Private FilteredData As Object

Private ButtonMatrix As Collection

Private CurrentButtonPanelIndex As Integer

Private InitialButtonPanelMargin As Long

Private ButtonTopMargin As Long
Private ButtonLeftMargin As Long
Private ButtonHeight As Long
Private ButtonWidth As Long

Private TmpRowHeight As Long
Private TmpColWidth As Long

Private i As Integer, FormLoaded As Boolean

Private Sub CmdBack_Click()
    PreviousButtonPanel
End Sub

Private Sub CmdClose_Click()
    Unload Me
End Sub

Private Sub LoadPhoneServiceType(pProductTypeCode As PhoneServices_ProductType)
    'Me.Visible = False
    FrmPhoneProducts.ProductType = pProductTypeCode
    FrmPhoneProducts.Show vbModal
    Set FrmPhoneProducts = Nothing
    'Me.Visible = True
End Sub

Private Sub CrearMatrizDeBotones(ByVal ButtonPanelItems As Integer)
    
    On Error GoTo Error
    
    Dim ButtonPanel As Collection
    
    If Not ButtonMatrix Is Nothing Then Exit Sub
    
    Set ButtonMatrix = New Collection
    
    i = 1
    
    While Not i > ImgProduct.UBound
        
        DoEvents
        
        If ButtonPanel Is Nothing Then Set ButtonPanel = New Collection
        
        ButtonPanel.Add i
        
        If (i Mod ButtonPanelItems) = 0 Or i = ImgProduct.UBound Then ButtonMatrix.Add ButtonPanel: Set ButtonPanel = Nothing
        
        i = i + 1
        
    Wend
    
    CmdBack.Visible = True
    CmdBack.Enabled = False
    CmdBack.ZOrder 0
    
    CmdNext.Visible = True
    CmdNext.Enabled = True
    CmdNext.ZOrder 0
    
    CurrentButtonPanelIndex = 1
    
    Exit Sub
    
Error:
    
    Debug.Print Err.Description
    
End Sub

Private Sub OrganizarBotones(Optional SetPanelIndex As Boolean = False)
    
    'MousePointer = vbHourglass
    
    Dim StartIndex As Integer, EndIndex As Integer
    
    If SetPanelIndex Then
        StartIndex = ButtonMatrix(CurrentButtonPanelIndex)(1)
        EndIndex = ButtonMatrix(CurrentButtonPanelIndex)(ButtonMatrix(CurrentButtonPanelIndex).Count)
    Else
        StartIndex = 1
        EndIndex = ImgProduct.UBound
    End If
            
    TmpRowHeight = 9360
    TmpColWidth = InitialButtonPanelMargin - ButtonLeftMargin
            
    For i = StartIndex To EndIndex
        'DoEvents
        TmpColWidth = AddButtonToRow(i, TmpColWidth)
        If (TmpRowHeight + ButtonHeight) > Me.Height Then
            CrearMatrizDeBotones i - 1
            Exit For
        End If
    Next i
    
    MousePointer = MousePointerConstants.vbDefault
    
End Sub

Private Function AddButtonToRow(ByVal pIndex As Integer, ByRef pPosicionAnterior As Long) As Long
    
    If (pPosicionAnterior + ButtonLeftMargin + ButtonWidth) > CmdNext.Left Then
        TmpRowHeight = TmpRowHeight + ButtonTopMargin + ButtonHeight
        pPosicionAnterior = InitialButtonPanelMargin - ButtonLeftMargin
    End If
    
    If (TmpRowHeight + ButtonHeight) > Me.Height Then AddButtonToRow = pPosicionAnterior: Exit Function
    
    LoadFileToControl PicLoad, FindPath(FilteredData(ImgProduct(pIndex).Tag)("ProductTypeID") & "\" & _
    FilteredData(ImgProduct(pIndex).Tag)("Code") & "\" & "ProductImage.png", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_ProductImageDir)
    
    Set ImgProduct(pIndex).Picture = PicLoad.Picture
    
    ImgProduct(pIndex).Top = TmpRowHeight
    
    ImgProduct(pIndex).Left = ButtonLeftMargin + pPosicionAnterior
    
    ImgProduct(pIndex).Height = ButtonHeight
    
    ImgProduct(pIndex).Width = ButtonWidth
    
    ImgProduct(pIndex).Visible = True
        
    AddButtonToRow = ImgProduct(pIndex).Left + ImgProduct(pIndex).Width
    
End Function

Private Sub PreviousButtonPanel()
    If CurrentButtonPanelIndex > 1 Then
        CurrentButtonPanelIndex = CurrentButtonPanelIndex - 1
        If CurrentButtonPanelIndex = 1 Then CmdBack.Enabled = False
        DisplayButtonPanel
        CmdNext.Enabled = True
    End If
End Sub

Private Sub NextButtonPanel()
    If CurrentButtonPanelIndex < ButtonMatrix.Count Then
        CurrentButtonPanelIndex = CurrentButtonPanelIndex + 1
        If CurrentButtonPanelIndex = ButtonMatrix.Count Then CmdNext.Enabled = False
        DisplayButtonPanel
        CmdBack.Enabled = True
    End If
End Sub

Private Sub DisplayButtonPanel()

    For i = ImgProduct.LBound To ImgProduct.UBound
        ImgProduct(i).Visible = False
    Next i
    
    OrganizarBotones True

End Sub

Private Sub CmdNext_Click()
    NextButtonPanel
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape, vbKeyF12
            If Not WebDoc.Visible Then
                CmdClose_Click
            Else
                CancelLongRunningOperation = True
            End If
    End Select
End Sub

Private Sub Form_Load()
        
    CancelLongRunningOperation = False
        
    TimerKeepAnimation.Enabled = False
    
    If Not ServiceReady Then
        Mensaje True, "The service is not available right now. Please verify your Internet Connection, the server connection availability and make sure it is currently handling external requests."
        Exit Sub
    End If
    
    InitialButtonPanelMargin = 1550
        
    ButtonTopMargin = 9360
    ButtonLeftMargin = 315
    ButtonWidth = 2835
    ButtonHeight = 1755
    
    CmdBack.Top = ButtonTopMargin + Fix((ButtonHeight / 2) - (CmdBack.Height / 2))
    CmdNext.Top = CmdBack.Top
    
    Call AjustarPantalla(Me)
    
End Sub

Private Sub Form_Activate()
    
If Not FormLoaded Then
        
    FormLoaded = True
    
    If Not ServiceReady Then
        Unload Me
        Exit Sub
    End If
    
    ' --------------------------
    
    Dim TmpFilePath As String, TmpFileContents As String, LastUpdate As Date
    
    TmpFilePath = FindPath("LastAutoUpdate.txt", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_ProductDataDir)
    
    TmpFileContents = LoadTextFile(TmpFilePath)
    
    If Trim(TmpFileContents) <> vbNullString Then
        
        On Error GoTo RunAgent
        
        LastUpdate = DecodeISOFullDate(TmpFileContents)
        
        If Abs(DateDiff("d", LastUpdate, Now)) > 0 Then GoTo RunAgent
        
        WebDoc.Visible = False
        
    Else
    
RunAgent:
        
        If Err.Number <> 0 Then Resume RunAgent
        
        On Error GoTo FinishAgent
        
        Dim PreviousLeft As Long, PreviousTop As Long, PreviousWidth As Long, PreviousHeight As Long
        
        PreviousLeft = Me.Left: PreviousTop = Me.Top: PreviousWidth = Me.Width: PreviousHeight = Me.Height
        
        TmpFilePath = FindPath("Spinner.gif", _
        NO_SEARCH_MUST_FIND_EXACT_MATCH)
        
        Me.Height = 500 * Screen.TwipsPerPixelY
        Me.Width = 500 * Screen.TwipsPerPixelX
        
        FrameStellarBar.Visible = False
        FrameDivTopSold.Visible = False
        FrameDivCategories.Visible = False
        
        Call AjustarPantalla(Me)
        
        Dim TmpHeight
        Dim TmpTop
        Dim TmpWidth
        Dim TmpLeft
        
        TmpHeight = Me.Height / Screen.TwipsPerPixelY
        TmpTop = (TmpHeight / 2) - (128 / 2)
        TmpWidth = Me.Width / Screen.TwipsPerPixelX
        TmpLeft = (TmpWidth / 2) - (128 / 2)
        
        'TmpFileContents = "<html><head><body style='overflow: hidden'><h1>Synchronizing data with Server... Please wait.</h1><img src = 'http://www.nasa.gov/multimedia/videogallery/ajax-loader.gif' style='height:100%;width:100%' /></body></head></html>"
        TmpFileContents = "<html><head><body style='overflow: hidden; position: relative'><h1 align='center'>Synchronizing data with Server</h1><h1 align='center'>Please Wait</h1><img style='position: absolute; top: " & TmpTop & "; left: " & TmpLeft & "' src = '" & TmpFilePath & "' /></body></head></html>"
        
        Dim TmpDoc As Object
    
        WebDoc.Left = -50
        WebDoc.Top = -50
        WebDoc.Height = Me.Height + 125
        WebDoc.Width = Me.Width + 125
        
        WebDoc.Resizable = False
        WebDoc.Navigate2 "About:Blank"
        
        Set TmpDoc = WebDoc.Document
        
        TmpDoc.open
        TmpDoc.Write TmpFileContents
        TmpDoc.Close
        
        'TimerKeepAnimation.Interval = 50
        'TimerKeepAnimation.Enabled = True
        
        PrepaidServicesLocalCls.UpdatePhoneProducts SerialNumber, ServiceVersionNumber, PrepaidRootDir, StellarProductID, PhoneServices_ProductImageDir, PhoneServices_ProductDataDir, PhoneServices_TopSoldProductsDir, False
        
        If CancelLongRunningOperation Then
            Unload Me
            Exit Sub
        End If
        
        'TimerKeepAnimation.Enabled = False
                
FinishAgent:
                
        WebDoc.Visible = False
        
        FrameStellarBar.Visible = True
        FrameDivTopSold.Visible = True
        FrameDivCategories.Visible = True
        
        Me.Width = PreviousWidth
        Me.Height = PreviousHeight
        
        Call AjustarPantalla(Me)
        
        DoEvents
                
        If Err.Number <> 0 Then Resume Continue
        
    End If
    
Continue:
    
    On Error GoTo 0
    
    ' --------------------------
    
    Dim ProductData As Object, Item As Object, TmpVar As Variant, i As Integer

    Set AllProductsData = New Collection

    ' --------------------------

    TmpFileContents = LoadTextFile(FindPath("ProductData.json", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_ProductDataDir & "/" & PhoneServices_ProductType.TypeDomesticTopup))
    
    Set ProductData = JSON.parse(TmpFileContents)
    
    If Not (ProductData Is Nothing) Then
        For Each TmpVar In ProductData
            Set Item = TmpVar
            Item.Add "ProductTypeID", TypeDomesticTopup
            AllProductsData.Add Item
        Next
    End If
    
    ' --------------------------
    
    TmpFileContents = LoadTextFile(FindPath("ProductData.json", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_ProductDataDir & "/" & PhoneServices_ProductType.TypePinless))
    
    Set ProductData = JSON.parse(TmpFileContents)
    
    If Not (ProductData Is Nothing) Then
        For Each TmpVar In ProductData
            Set Item = TmpVar
            Item.Add "ProductTypeID", TypePinless
            AllProductsData.Add Item
        Next
    End If
    
    ' --------------------------
    
    TmpFileContents = LoadTextFile(FindPath("ProductData.json", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_ProductDataDir & "/" & PhoneServices_ProductType.TypeLongDistance))
    
    Set ProductData = JSON.parse(TmpFileContents)
    
    If Not (ProductData Is Nothing) Then
        For Each TmpVar In ProductData
            Set Item = TmpVar
            Item.Add "ProductTypeID", TypeLongDistance
            AllProductsData.Add Item
        Next
    End If
    
    ' --------------------------
    
    TmpFileContents = LoadTextFile(FindPath("ProductData.json", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_ProductDataDir & "/" & PhoneServices_ProductType.TypeInternationalTopup))
    
    Set ProductData = JSON.parse(TmpFileContents)
    
    If Not (ProductData Is Nothing) Then
        For Each TmpVar In ProductData
            Set Item = TmpVar
            Item.Add "ProductTypeID", TypeInternationalTopup
            AllProductsData.Add Item
        Next
    End If
    
    ' --------------------------
    
    TmpFileContents = LoadTextFile(FindPath("TopSoldItems.json", _
    NO_SEARCH_MUST_FIND_EXACT_MATCH, PhoneServices_TopSoldProductsDir))
    
    Set TopSoldProductsData = JSON.parse(TmpFileContents)
    
    ' --------------------------
    
    Set FilteredData = New Collection
    
    If Not TopSoldProductsData Is Nothing Then
        
        For Each TmpVar In AllProductsData
            
            Set Item = TmpVar
            
            If ProductBelongsInTopSold(Item("Code")) Then
            
                Collection_AddKey FilteredData, Item, Item("Code")
            
            End If
            
        Next
    
    End If
    
    ' --------------------------
    
    LimpiarBotones
    
    i = 0
    
    For Each Item In FilteredData
    
        i = i + 1
        
        Load ImgProduct(i)
        ImgProduct(i).Tag = Item("Code")
    
    Next
    
    OrganizarBotones
    
End If
    
End Sub

Private Function ProductBelongsInTopSold(ByVal ProductCode As String) As Boolean

    For Each Item In TopSoldProductsData
        If UCase(Item("ProductCode")) = UCase(ProductCode) Then
            ProductBelongsInTopSold = True
            Exit Function
        End If
    Next

End Function

Private Sub LimpiarBotones()

    On Error Resume Next

    For i = 1 To ImgProduct.UBound
        Unload ImgProduct(i)
    Next i
    
    CmdBack.Visible = False: CmdNext.Visible = False
    
End Sub

Private Sub FrameProductLongDistance_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    LoadPhoneServiceType TypeLongDistance
End Sub

Private Sub ImgDomesticTopup_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    LoadPhoneServiceType TypeDomesticTopup
End Sub

Private Sub ImgInternationalTopup_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    LoadPhoneServiceType TypeInternationalTopup
End Sub

Private Sub ImgLongDistance_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    LoadPhoneServiceType TypeLongDistance
End Sub

Private Sub ImgPinless_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    LoadPhoneServiceType TypePinless
End Sub

Private Sub ImgProduct_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then
        
        Set SelectedProductData = FilteredData(ImgProduct(Index).Tag)
        'MsgBox SelectedProductData("CarrierName")
                
        Set FrmPhoneProducts_Order.ProductData = SelectedProductData
        Set FrmPhoneProducts_Order.ProductImage = ImgProduct(Index).Picture
        FrmPhoneProducts_Order.ProductType = SelectedProductData("ProductTypeID")
        FrmPhoneProducts_Order.Show vbModal
        Set FrmPhoneProducts_Order = Nothing
        
    Else
        ' Let Right Click automatically display Tooltip...
    End If
End Sub

Private Sub lblProductLongDistance_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    FrameProductLongDistance_MouseUp 0, 0, 0, 0
End Sub

Private Sub FrameProductPinless_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    LoadPhoneServiceType TypePinless
End Sub

Private Sub lblProductPinless_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    FrameProductPinless_MouseUp 0, 0, 0, 0
End Sub

Private Sub FrameProductDomesticTopup_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    LoadPhoneServiceType TypeDomesticTopup
End Sub

Private Sub lblProductDomesticTopup_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    FrameProductDomesticTopup_MouseUp 0, 0, 0, 0
End Sub

Private Sub FrameProductInternationalTopup_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    LoadPhoneServiceType TypeInternationalTopup
End Sub

Private Sub lblProductInternationalTopup_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    FrameProductInternationalTopup_MouseUp 0, 0, 0, 0
End Sub

Private Sub TimerKeepAnimation_Timer()
    DoEvents
End Sub
