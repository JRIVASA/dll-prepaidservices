VERSION 5.00
Begin VB.Form FrmTestDLL 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   2490
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   2850
   Icon            =   "FrmTestDLL.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2490
   ScaleWidth      =   2850
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdExit 
      Caption         =   "Exit"
      Height          =   855
      Left            =   360
      TabIndex        =   1
      Top             =   1320
      Width           =   2175
   End
   Begin VB.CommandButton CmdPhoneServices 
      Caption         =   "Sync Phone Services"
      Height          =   855
      Left            =   360
      TabIndex        =   0
      Top             =   240
      Width           =   2175
   End
End
Attribute VB_Name = "FrmTestDLL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdExit_Click()
    Unload Me
End Sub

Private Sub CmdPhoneServices_Click()
    
    On Error GoTo Error
    
With PrepaidBlackstone
    
    Dim Cls As Object
    Set Cls = CreateObject("BlackstonePrepaid.clsPrepaidServices")
    Cls.UpdatePhoneProducts .SerialNumber, .ServiceVersionNumber, .PrepaidRootDirectory, CLng(PC), .PhoneServices_ImageDirectory, .PhoneServices_DataDirectory, .PhoneServices_TopSoldProductsDirectory
    
End With
    
Error:
    
    EndTime = Now
    
    MsgBox Err.Description
    
    sWriteIni gPathSetup, "LastExecutionError", "State", "Finished"
    sWriteIni gPathSetup, "LastExecutionError", "Number", Err.Number
    sWriteIni gPathSetup, "LastExecutionError", "Description", Err.Description
    sWriteIni gPathSetup, "LastExecutionError", "Source", Err.Source
    sWriteIni gPathSetup, "LastExecutionError", "Started", FormatDateTime(InitTime, vbGeneralDate)
    sWriteIni gPathSetup, "LastExecutionError", "Finished", FormatDateTime(EndTime, vbGeneralDate)
    sWriteIni gPathSetup, "LastExecutionError", "Runtime", IIf((EndTime - InitTime) > 1, Format((EndTime - InitTime) + 1, "d") & "D ", vbNullString) & Format((EndTime - InitTime), "HH:mm:ss")
    
    End
    
End Sub
