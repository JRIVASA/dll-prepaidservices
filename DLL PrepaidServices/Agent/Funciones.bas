Attribute VB_Name = "Funciones"
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpString _
    As Any, ByVal lpFileName As String) As Long
    
Public InitTime As Date
Public EndTime As Date
    
Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Sub GetSystemInfo Lib "kernel32" (lpSystemInfo As SYSTEM_INFO)

Private Type SYSTEM_INFO
    dwOemID As Long
    dwPageSize As Long
    lpMinimumApplicationAddress As Long
    lpMaximumApplicationAddress As Long
    dwActiveProcessorMask As Long
    dwNumberOrfProcessors As Long
    dwProcessorType As Long
    dwAllocationGranularity As Long
    dwReserved As Long
End Type

Public Type PB_PS_ResultTicket
    
    MaxCharsPerLine             As Long
    BaseFilePath                As String
    OptionalHeaderFilePath      As String
    
    TmpFilePath                 As String
    TmpHeader                   As String
    
End Type

Public Type PB_PS_ProductTypeCodes
    
    LongDistanceStellarCode As String
    PinlessStellarCode As String
    DomesticTopupStellarCode As String
    InternationalTopupStellarCode As String
    SunpassStellarCode As String
    
End Type

Public Type PrepaidBlackstoneSettings
    
    SerialNumber                                            As String
    ServiceVersionNumber                                    As String
    
    PrepaidRootDirectory                                    As String
    
    PhoneServices_ImageDirectory                            As String
    PhoneServices_DataDirectory                             As String
    PhoneServices_TopSoldProductsDirectory                  As String
    
    StellarProductCode                                      As Long
    
    ProductCodes                                            As PB_PS_ProductTypeCodes
    
    ResultTicket                                            As PB_PS_ResultTicket
    
    Active                                                  As Boolean
    
End Type

Public PrepaidBlackstone As PrepaidBlackstoneSettings

Public gPathSetup As String

Const PC = 859

Public Function sGetIni(SIniFile As String, SSection As String, _
sKey As String, SDefault As String) As String
    
    Const ParamMaxLength = 10000
    
    Dim STemp As String * ParamMaxLength
    Dim NLength As Integer
    
    STemp = Space$(ParamMaxLength)
    
    NLength = GetPrivateProfileString(SSection, sKey, SDefault, STemp, ParamMaxLength, SIniFile)
    
    sGetIni = Left$(STemp, NLength)
    
End Function

Public Sub sWriteIni(SIniFile As String, SSection As String, _
sKey As String, sData As String)
    WritePrivateProfileString SSection, sKey, sData, SIniFile
End Sub

Sub Main()

    InitTime = Now
    
    On Error GoTo Error
    
    Dim TipoInterfaz As Integer
    
    gPathSetup = App.Path & "\Setup.ini"
    
    sWriteIni gPathSetup, "LastExecutionError", "Number", "N/A"
    sWriteIni gPathSetup, "LastExecutionError", "Description", "N/A"
    sWriteIni gPathSetup, "LastExecutionError", "Source", "N/A"
    sWriteIni gPathSetup, "LastExecutionError", "Finished", "N/A"
    sWriteIni gPathSetup, "LastExecutionError", "Runtime", "N/A"
    
    sWriteIni gPathSetup, "LastExecutionError", "Started", FormatDateTime(InitTime, vbGeneralDate)
    sWriteIni gPathSetup, "LastExecutionError", "State", "Running"
    
    TipoInterfaz = Fix(Val(sGetIni(gPathSetup, "General", "Interface", "-1")))
    
With PrepaidBlackstone
                           
    .StellarProductCode = PC
               
    .SerialNumber = sGetIni(gPathSetup, "BlackstonePrepaid", "SerialNumber", "")
    .ServiceVersionNumber = sGetIni(gPathSetup, "BlackstonePrepaid", "ServiceVersionNumber", "1")
    .PrepaidRootDirectory = sGetIni(gPathSetup, "BlackstonePrepaid", "RootDirectory", vbNullString) ' Algo como \\NetworkFolder\BlackstonePrepaid
    .PhoneServices_DataDirectory = sGetIni(gPathSetup, "BlackstonePrepaid", "PhoneServices_DataDirectory", vbNullString) ' Algo como \\NetworkFolder\BlackstonePrepaid\Data\Phone Services
    .PhoneServices_ImageDirectory = sGetIni(gPathSetup, "BlackstonePrepaid", "PhoneServices_ImageDirectory", vbNullString) ' Algo como \\NetworkFolder\BlackstonePrepaid\Images\Phone Services
    .PhoneServices_TopSoldProductsDirectory = sGetIni(gPathSetup, "BlackstonePrepaid", "PhoneServices_TopSoldProductsDirectory", vbNullString) ' Puede ser el mismo que el Data Directory a menos que se requiera otra estructura. O siguiendo el mismo esquema estructural, algo como \\NetworkFolder\BlackstonePrepaid\Top Sold\Phone Services
        
    '.ProductCodes.LongDistanceStellarCode = sGetIni(gPathSetup, "BlackstonePrepaid", "LongDistanceProductCode", vbNullString)
    '.ProductCodes.PinlessStellarCode = sGetIni(gPathSetup, "BlackstonePrepaid", "PinlessProductCode", vbNullString)
    '.ProductCodes.DomesticTopupStellarCode = sGetIni(gPathSetup, "BlackstonePrepaid", "DomesticTopupProductCode", vbNullString)
    '.ProductCodes.InternationalTopupStellarCode = sGetIni(gPathSetup, "BlackstonePrepaid", "InternationalTopupProductCode", vbNullString)
    '.ProductCodes.SunpassStellarCode = sGetIni(gPathSetup, "BlackstonePrepaid", "SunpassProductCode", vbNullString)
    
    '.ResultTicket.BaseFilePath = sGetIni(gPathSetup, "BlackstonePrepaid", "ResultTicketFilePath", vbNullString) ' Algo como \\NetworkFolder\BlackstonePrepaid\ResultTickets\Phone Services\$(ComputerName)\LastTicket.txt
    '' Variables opcionales a utilizar en la ruta [ $(ComputerName): Nombre del Equipo | $(TicketNumber): Numero del Ticket ]
    '.ResultTicket.BaseFilePath = Replace(.ResultTicket.BaseFilePath, "$(ComputerName)", NombreDelComputador)
    '.ResultTicket.OptionalHeaderFilePath = sGetIni(gPathSetup, "BlackstonePrepaid", "ResultTicketHeaderFilePath", vbNullString) ' Algo como \\NetworkFolder\BlackstonePrepaid\ResultTickets\Header.txt
    '.ResultTicket.MaxCharsPerLine = Val(sGetIni(gPathSetup, "BlackstonePrepaid", "ResultTicketMaxLineCharacters", "32"))
            
    .Active = (.SerialNumber <> vbNullString) And (.PrepaidRootDirectory <> vbNullString) And (.PhoneServices_DataDirectory <> vbNullString) And (.PhoneServices_ImageDirectory <> vbNullString) And (.PhoneServices_TopSoldProductsDirectory <> vbNullString)
    
    If Not .Active Then Err.Raise 1000, "Initializing", "Missing Setup Required Data"
    
    If TipoInterfaz = 0 Then
        
        ' Ser�a una interfaz con botones para actualizar tipos de productos de Blackstone. Nada mas esta integrado servicios telef�nicos.
        
        FrmTestDLL.Show vbModal
            
    ' A partir de aqu� los distintos tipos de interfaz mayores a cero (0)
    ' ser�n agentes sin interfaz que actualizan cada uno ciertas cosas.
    
    ElseIf TipoInterfaz = 1 Then ' Actualizar servicios telef�nicos
        
        Dim Cls As Object
        Set Cls = CreateObject("BlackstonePrepaid.clsPrepaidServices")
        Cls.UpdatePhoneProducts .SerialNumber, .ServiceVersionNumber, .PrepaidRootDirectory, CLng(PC), .PhoneServices_ImageDirectory, .PhoneServices_DataDirectory, .PhoneServices_TopSoldProductsDirectory
                
    Else
        Err.Raise 2000, "Initializing", "Missing Setup Required Data: ""[General]->Interface"" Undefined"
    End If
    
    EndTime = Now
    
    sWriteIni gPathSetup, "LastExecutionError", "State", "Finished"
    sWriteIni gPathSetup, "LastExecutionError", "Number", "N/A"
    sWriteIni gPathSetup, "LastExecutionError", "Description", "None - Execution OK"
    sWriteIni gPathSetup, "LastExecutionError", "Source", "N/A"
    sWriteIni gPathSetup, "LastExecutionError", "Started", FormatDateTime(InitTime, vbGeneralDate)
    sWriteIni gPathSetup, "LastExecutionError", "Finished", FormatDateTime(EndTime, vbGeneralDate)
    sWriteIni gPathSetup, "LastExecutionError", "Runtime", IIf((EndTime - InitTime) > 1, Format((EndTime - InitTime) + 1, "d") & "D ", vbNullString) & Format((EndTime - InitTime), "HH:mm:ss")
    
End With
    
    Exit Sub
    
Error:

    EndTime = Now
    
    sWriteIni gPathSetup, "LastExecutionError", "State", "Finished"
    sWriteIni gPathSetup, "LastExecutionError", "Number", Err.Number
    sWriteIni gPathSetup, "LastExecutionError", "Description", Err.Description
    sWriteIni gPathSetup, "LastExecutionError", "Source", Err.Source
    sWriteIni gPathSetup, "LastExecutionError", "Started", FormatDateTime(InitTime, vbGeneralDate)
    sWriteIni gPathSetup, "LastExecutionError", "Finished", FormatDateTime(EndTime, vbGeneralDate)
    sWriteIni gPathSetup, "LastExecutionError", "Runtime", IIf((EndTime - InitTime) > 1, Format((EndTime - InitTime) + 1, "d") & "D ", vbNullString) & Format((EndTime - InitTime), "HH:mm:ss")
    
End Sub

Public Function NombreDelComputador() As String
    Dim Buffers As String * 255, Tamano As Long
    Tamano = GetComputerName(Buffers, 255)
    NombreDelComputador = Replace(Buffers, Chr(0), "")
End Function

Public Function SerialDelProcesador() As Long
    Dim Registro As SYSTEM_INFO
    Call GetSystemInfo(Registro)
    SerialDelProcesador = Registro.dwReserved
End Function
