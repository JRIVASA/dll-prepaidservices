Attribute VB_Name = "Variables"
Global PrepaidRootDir As String
Global PhoneServices_ProductImageDir As String
Global PhoneServices_ProductDataDir As String
Global PhoneServices_TopSoldProductsDir As String

Global StellarProductID As Long
Global StellarADMConnection As Object
Global StellarPOSConnection As Object

Global WindowsArchitecture As OperatingSystemArchitecture

Global PrepaidServicesLocalCls As clsPrepaidServices
Global ServiceReady As Boolean

Global ServiceVersionNumber As String

Global SerialNumber As String
Global MerchantID As String
Global TerminalID As String
Global CanReleaseOrders As Boolean

Global DLLResponseDataObject As Object

Global Retorno As Boolean

Global CancelLongRunningOperation As Boolean
